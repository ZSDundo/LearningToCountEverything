# Learning To Count Everything
![image](learn2countEverything.png)

本代码在官方源码的基础上，添加了中文注释，并对代码结构做了微调。

官方源码地址：https://github.com/cvlab-stonybrook/LearningToCountEverything

论文地址: https://arxiv.org/pdf/2104.08391.pdf

## Dataset download 
图像数据集下载地址(包含密度图): https://drive.google.com/file/d/1ymDYrGs9DSRicfZbSCDiOu0ikGDh5k6S/view?usp=sharing

密度图下载地址: https://archive.org/details/FSC147-GT

请将解压后的图像数据集目录和密度图目录放在data目录下。


## 配置环境
```shell
conda create -n fscount python=3.7 -y

conda activate fscount

python -m pip install matplotlib opencv-python notebook tqdm

conda install pytorch==1.4.0 torchvision==0.5.0 cudatoolkit=10.0 -c pytorch
```

## Quick demo

提供了一个输入图像，并在txt文件中提供exemplar对象的边界框。


``` bash
python demo.py --input-image orange.jpg --bbox-file orange_box_ex.txt 
```

Use our provided interface to specify the bounding boxes for exemplar objects


``` bash
python demo.py --input-image orange.jpg
```


## 评估

提供了预训练的FamNet模型，评估代码可以在不进行训练的情况下使用。

### 在验证集上进行测试，不进行适应性调整
```bash 
python test.py --data_path /PATH/TO/YOUR/FSC147/DATASET/ --test_split val
```
### 在验证集上进行测试，并进行适应性调整
```bash 
python test.py --data_path /PATH/TO/YOUR/FSC147/DATASET/ --test_split val --adapt
```


## 训练 
``` bash
python train.py --gpu 0
```



