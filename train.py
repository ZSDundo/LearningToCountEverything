"""
Training Code for Learning To Count Everything, CVPR 2021
Authors: Viresh Ranjan,Udbhav, Thu Nguyen, Minh Hoai

Last modified by: Minh Hoai Nguyen (minhhoai@cs.stonybrook.edu)
Date: 2021/04/19
"""
import torch.nn as nn
from model import Resnet50FPN, CountRegressor, weights_normal_init
from utils import MAPS, Scales, Transform, TransformTrain, extract_features, visualize_output_and_save
from PIL import Image
import os
import torch
import argparse
import json
import numpy as np
from tqdm import tqdm
from os.path import exists, join
import random
import torch.optim as optim
import torch.nn.functional as F


# 相关配置参数
def get_args():
    parser = argparse.ArgumentParser(description="Few Shot Counting Evaluation code")
    parser.add_argument("-dp", "--data_path", type=str, default="D:\\python\\deepbule\\kaggle\\FSC147_384_V2\\", help="Path to the FSC147 dataset")
    parser.add_argument("-o", "--output_dir", type=str, default="./logsSave", help="/Path/to/output/logs/")
    parser.add_argument("-ts", "--test-split", type=str, default='val', choices=["train", "test", "val"],
                        help="what data split to evaluate on on")
    parser.add_argument("-ep", "--epochs", type=int, default=1500, help="number of training epochs")
    parser.add_argument("-g", "--gpu", type=int, default=0, help="GPU id")
    parser.add_argument("-lr", "--learning-rate", type=float, default=1e-5, help="learning rate")
    args = parser.parse_args()
    return args

# 加载数据
def load_data(args):
    data_path = args.data_path
    # 每一张图片少量的exemplar object的bbox信息(box_examples_coordinates)，以及对应点标注信息(points)，原始图片的宽高等信息
    anno_file = data_path + 'annotation_FSC147_384.json'
    # 训练集和测试集分别含有的图片名
    data_split_file = data_path + 'Train_Test_Val_FSC_147.json'
    # 输入图像文件夹途径
    im_dir = data_path + 'images_384_VarV2'
    # 密度图文件夹途径
    gt_dir = data_path + 'gt_density_map_adaptive_384_VarV2'

    if not exists(args.output_dir):
        os.mkdir(args.output_dir)

    with open(anno_file) as f:
        # 加载标注信息
        annotations = json.load(f)
    with open(data_split_file) as f:
        # 加载数据集划分信息
        data_split = json.load(f)
    return annotations, data_split, im_dir, gt_dir


# 模型训练
def train_one_epoch(args, resnet50_conv, optimizer, regressor, criterion, device):
    print("Training on FSC147 train set data")
    # 加载数据集及相关信息
    annotations, data_split, im_dir, gt_dir = load_data(args)
    # 训练数据集中图片名称, 如['7.jpg',...]
    im_ids = data_split['train']
    random.shuffle(im_ids)
    train_mae = 0
    train_rmse = 0
    train_loss = 0
    cnt = 0

    pbar = tqdm(im_ids)
    for im_id in pbar:
        cnt += 1
        anno = annotations[im_id]
        bboxes = anno['box_examples_coordinates']
        # 1、获取每幅图片上少量的exemplar object的bbox信息
        rects = list()
        for bbox in bboxes:
            x1 = bbox[0][0]
            y1 = bbox[0][1]
            x2 = bbox[2][0]
            y2 = bbox[2][1]
            rects.append([y1, x1, y2, x2])
        # 2、加载图片
        image = Image.open('{}/{}'.format(im_dir, im_id))
        image.load()
        # 3、加载密度图
        density_path = gt_dir + '/' + im_id.split(".jpg")[0] + ".npy"
        density = np.load(density_path).astype('float32')
        # 4、装入到字典中
        sample = {'image': image,
                  'lines_boxes': rects,
                  'gt_density': density}
        # 训练过程中，对图像进行预处理
        sample = TransformTrain(sample)
        image, boxes, gt_density = sample['image'].to(device), sample['boxes'].to(device), sample['gt_density'].to(device)

        with torch.no_grad():
            # 获取不同尺度的correlation map，并concat在一起
            features = extract_features(resnet50_conv, image.unsqueeze(0), boxes.unsqueeze(0), MAPS, Scales)
        features.requires_grad = True
        optimizer.zero_grad()
        # 将6个correlation map进行concat，然后输入 密度图预测模块
        output = regressor(features)

        # if image size isn't divisible by 8, gt size is slightly different from output size
        if output.shape[2] != gt_density.shape[2] or output.shape[3] != gt_density.shape[3]:
            orig_count = gt_density.sum().detach().item()
            gt_density = F.interpolate(gt_density, size=(output.shape[2], output.shape[3]), mode='bilinear')
            new_count = gt_density.sum().detach().item()
            if new_count > 0:
                # 保证gt_density缩放后的count和orig_count相同
                gt_density = gt_density * (orig_count / new_count)
        # 计算mse loss
        loss = criterion(output, gt_density)
        loss.backward()
        optimizer.step()
        train_loss += loss.item()
        pred_cnt = torch.sum(output).item()
        gt_cnt = torch.sum(gt_density).item()
        cnt_err = abs(pred_cnt - gt_cnt)
        # 计算训练过程中mae和rmse
        train_mae += cnt_err
        train_rmse += cnt_err ** 2
        pbar.set_description('真实cnt: {:6.1f}, 预测cnt: {:6.1f}, 错误个数: {:6.1f}. Current MAE: {:5.2f}, RMSE: {:5.2f}'.format(gt_cnt, pred_cnt,
                                                                        abs(pred_cnt - gt_cnt), train_mae/cnt, (train_rmse/cnt)**0.5))
    # 计算训练1个epoch的train_loss、train_mae、train_rmse
    train_loss = train_loss / len(im_ids)
    train_mae = (train_mae / len(im_ids))
    train_rmse = (train_rmse / len(im_ids))**0.5
    return train_loss, train_mae, train_rmse


# 模型评估
def eval(args, resnet50_conv, regressor, device):
    annotations, data_split, im_dir, gt_dir = load_data(args)

    cnt = 0
    SAE = 0 # sum of absolute errors
    SSE = 0 # sum of square errors

    print("Evaluation on {} data".format(args.test_split))
    im_ids = data_split[args.test_split]
    pbar = tqdm(im_ids)
    for im_id in pbar:
        anno = annotations[im_id]
        bboxes = anno['box_examples_coordinates']
        dots = np.array(anno['points'])

        rects = list()
        for bbox in bboxes:
            x1 = bbox[0][0]
            y1 = bbox[0][1]
            x2 = bbox[2][0]
            y2 = bbox[2][1]
            rects.append([y1, x1, y2, x2])

        image = Image.open('{}/{}'.format(im_dir, im_id))
        image.load()
        sample = {'image': image,'lines_boxes': rects}
        sample = Transform(sample)
        image, boxes = sample['image'].to(device), sample['boxes'].to(device)

        with torch.no_grad():
            output = regressor(extract_features(resnet50_conv, image.unsqueeze(0), boxes.unsqueeze(0), MAPS, Scales))

        gt_cnt = dots.shape[0]
        pred_cnt = output.sum().item()
        cnt = cnt + 1
        err = abs(gt_cnt - pred_cnt)
        SAE += err
        SSE += err**2

        pbar.set_description('{:<8}: 真实cnt-预测cnt: {:6d}-{:6.1f}, error: {:6.1f}. Current MAE: {:5.2f}, RMSE: {:5.2f}'.format(im_id,
                                                        gt_cnt, pred_cnt, abs(pred_cnt - gt_cnt), SAE/cnt, (SSE/cnt)**0.5))
    print("")
    print('On {} data, MAE: {:6.2f}, RMSE: {:6.2f}'.format(args.test_split, SAE/cnt, (SSE/cnt)**0.5))
    return SAE/cnt, (SSE/cnt)**0.5


def train(args):
    device = torch.device(args.gpu if torch.cuda.is_available() else "cpu")

    # MSE损失函数
    criterion = nn.MSELoss().to(device)

    # 1、多尺度特征提取模块
    resnet50_conv = Resnet50FPN().to(device)
    resnet50_conv.eval()
    # 2、密度图预测模块
    # input_channels=6，这是因为map3和map4分别产生三个尺度（0.9、1.0、1.1）的correlation map
    regressor = CountRegressor(input_channels=6, pool='mean').to(device)
    weights_normal_init(regressor, dev=0.001)
    regressor.train()


    # 3、这里冻结Resnet50FPN，只更新CountRegressor的参数
    optimizer = optim.Adam(regressor.parameters(), lr=args.learning_rate)

    best_mae, best_rmse = 1e7, 1e7
    stats = list()
    for epoch in range(0, args.epochs):
        regressor.train()
        # 训练1个epoch
        train_loss, train_mae, train_rmse = train_one_epoch(args, resnet50_conv, optimizer, regressor, criterion, device)
        regressor.eval()
        # 模型评估
        val_mae, val_rmse = eval(args, resnet50_conv, regressor, device)
        # 将训练过程中的loss等信息保存到文件中
        stats.append((train_loss, train_mae, train_rmse, val_mae, val_rmse))
        stats_file = join(args.output_dir, "stats" + ".txt")
        with open(stats_file, 'w') as f:
            for s in stats:
                f.write("%s\n" % ','.join([str(x) for x in s]))
        if best_mae < val_mae:
            best_mae = val_mae
            best_rmse = val_rmse
            model_name = args.output_dir + '/' + f"FamNet_{best_mae}.pth"
            torch.save(regressor.state_dict(), model_name)
        print("Epoch {}, Avg. Epoch Loss: {} Train MAE: {} Train RMSE: {} Val MAE: {} Val RMSE: {} Best Val MAE: {} Best Val RMSE: {} ".format(
                epoch + 1, stats[-1][0], stats[-1][1], stats[-1][2], stats[-1][3], stats[-1][4], best_mae, best_rmse))


if __name__ == '__main__':
    args = get_args()
    train(args)




    




